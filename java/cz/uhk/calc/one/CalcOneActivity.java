package cz.uhk.calc.one;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Aktivita jednoduche kalkulacky s programove generovanym GUI
 * Neni implementovan vypocet
 *
 * @author Tomas Kozel
 */
public class CalcOneActivity extends Activity {
    final static String[] BT_LABELS = {
            "7", "8", "9", "*",
            "4", "5", "6", "/",
            "1", "2", "3", "+",
            "C", "0", "=", "-"

    };

    TextView display;
    Button[] btns = new Button[16];

    //vyraz tvoreny kalkulackou
    StringBuilder vyraz = new StringBuilder();

    private boolean dalsiMaze = true;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();

        initGui(ctx);
    }


    void initGui(Context ctx) {
        TableLayout tab = new TableLayout(this);
        setContentView(tab);

        TableLayout.LayoutParams rowParams = new TableLayout.LayoutParams(-1, -1, 0.1f);
        TableRow firstRow = new TableRow(ctx);
        //POZOR Child komponenta musi pouyivat layout parametry vlastnika (Parent) ->
        //  pro radek tedy bude pouzit TableLayout.LayoutParams !!!!
        firstRow.setLayoutParams(rowParams);
        tab.addView(firstRow);

        display = new TextView(ctx);
        display.setTextSize(TypedValue.COMPLEX_UNIT_PT, 20);
        display.setText("0");
        display.setMaxLines(1);
        display.setTextColor(Color.BLACK);
        display.setGravity(Gravity.CENTER_VERTICAL + Gravity.RIGHT);
        display.setBackgroundColor(Color.rgb(0, 0xcc, 0x66));
        display.setPadding(10, 0, 10, 0);

        TableRow.LayoutParams dispParams = new TableRow.LayoutParams(-1, -1, 1);
        dispParams.span = 4; //pres 4 sloupce
        dispParams.setMargins(10, 10, 10, 10);
        display.setLayoutParams(dispParams);
        firstRow.addView(display);

        TableRow row = null;
        TableRow.LayoutParams cellParams = new TableRow.LayoutParams(-1, -1, 1);
        rowParams = new TableLayout.LayoutParams(-1, -1, 0.225f);
        //spolecny listener tlacitek
        OnClickListener buttonListener = new OnClickListener() {

            @Override
            public void onClick(View view) {
                processClick(view);
            }
        };
        for (int i = 0; i < 16; i++) {
            if (i % 4 == 0) {
                //pro kazda 4 tlacitka novy radek
                row = new TableRow(ctx);
                row.setLayoutParams(rowParams);
                row.setBaselineAligned(true);
                tab.addView(row);
            }
            Button bt = new Button(ctx);
            bt.setLayoutParams(cellParams);
            bt.setText(BT_LABELS[i]);
            bt.setTextSize(TypedValue.COMPLEX_UNIT_PT, 16);
            bt.setOnClickListener(buttonListener);
            btns[i] = bt;
            row.addView(btns[i]);
        }
    }


    /**
     * Remove leading zeros
     *
     * @param sb
     */
    // private int removeLeadZeros(StringBuilder sb) {
    // }


    /**
     * Zpracovani udalosti tlacitek
     *
     * @param v
     */
    protected void processClick(View v) {

        Button b = (Button) v;
        char c = b.getText().charAt(0); //prvni a jediny znak
        String dispStr = display.getText().toString();
        if (Character.isDigit(c)) {
            //cislo
            if (dalsiMaze) {
                dispStr = String.valueOf(c);
                vyraz.append(c);
                dalsiMaze = false;
            } else {
                vyraz.append(c);
            }

            // remove leading zeros
            if (vyraz.substring(0, 1) == "0") {
                String temp = vyraz.substring(1);
                vyraz.setLength(0);
                vyraz.append(temp);
            }

            display.setText(vyraz.toString());

        } else {
            char lastButOne = vyraz.charAt(vyraz.length() - 1);

            if (c == '=' && !(lastButOne == '+' || lastButOne == '-'
                    || lastButOne == '*' || lastButOne == '/')) {

                String strVyraz = vyraz.toString();
                vyraz.setLength(0);
                char op = Character.MIN_VALUE;
                char[] opsArray = "+-/*".toCharArray();

                for (char ch : opsArray) {
                    if (strVyraz.contains(Character.toString(ch))) {
                        op = ch;
                        break;
                    }
                }

                int result = 0;

                // locate j - index of operator in strVyraz
                int j = strVyraz.indexOf(op);

                int operand1 = Integer.parseInt(strVyraz.substring(0, j));
                int operand2 = Integer.parseInt(strVyraz.substring(j + 1, strVyraz.length()));

                switch (op) {
                    case '+': result = operand1 + operand2;
                        break;
                    case '-': result = operand1 - operand2;
                        break;
                    case '*': result = operand1 * operand2;
                        break;
                    case '/': result = operand1 / operand2;
                        break;
                }

                display.setText(Integer.toString( result ));
                vyraz.append(result);

            }

            if (c == '=' && (lastButOne == '+' || lastButOne == '-'
                    || lastButOne == '*' || lastButOne == '/')) {
                vyraz = new StringBuilder();
                dalsiMaze = true;
                display.setText("0");
            }

            else if (c == 'C') {
                vyraz = new StringBuilder();
                dalsiMaze = true;
                display.setText("0");
            }

            if ("+-/*".indexOf(c) != -1) {
                vyraz.append(c);
                // here:
                // display.setText(removeLeadZeros(vyraz.toString()));
                display.setText(vyraz.toString());
                dalsiMaze = true;
            }

            // TODO it's possible to remove leading zeros '002 + 03 = 5' (I may not have time to do it soon).
        }
    }
}